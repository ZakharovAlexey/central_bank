CREATE TABLE POST (
  id                        INTEGER PRIMARY KEY,
  profession                varchar (50)

);
CREATE TABLE EMPLOYEE (
id                         INTEGER PRIMARY KEY,
post_id                    INTEGER REFERENCES POST (id),
FULL_NAME                  varchar(50) NOT NULL
);


CREATE TABLE REPORT (
id                        INTEGER PRIMARY KEY,
employee_id               INTEGER REFERENCES EMPLOYEE (id),
date_of_absent            TIMESTAMP,
how_long_time_is_absent   FLOAT (50),
reason                    varchar (200)

);
