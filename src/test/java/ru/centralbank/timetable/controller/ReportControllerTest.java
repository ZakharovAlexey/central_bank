package ru.centralbank.timetable.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.centralbank.timetable.controller.config.ReportControllerTestConfig;
import ru.centralbank.timetable.dto.ReportDto;
import ru.centralbank.timetable.model.Employee;
import ru.centralbank.timetable.model.Post;
import ru.centralbank.timetable.service.ReportService;
import ru.centralbank.timetable.service.converter.EmployeeDtoConverter;

import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ReportControllerTestConfig.class)
@WebAppConfiguration
public class ReportControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private ReportService reportService;
    @Autowired
    private EmployeeDtoConverter employeeDtoConverter;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        initMocks(this);

    }

    @Test
    public void testAddReport_success() throws Exception {
        ReportDto reportDto = provideReportDTO();
        doReturn(1).when(reportService).addReport(reportDto);
        mockMvc.perform(post("/report")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(reportDto)))
                .andExpect(status().isOk());
    }

    @Test
    public void testRemoveReport_Returns200() throws Exception {
        mockMvc.perform(delete("/report/1")).andExpect(status().isOk());
    }

    private ReportDto provideReportDTO() {
        return ReportDto.builder()
                .employee(employeeDtoConverter.convertToDto(getEmployee()))
                .dateOfAbsent(null)
                .howLongTimeIsAbsent(40f)
                .reason("oversleep")
                .build();
    }

    private Employee getEmployee() {
        return Employee.builder()
                .post(new Post(1, "Doctor"))
                .fullName("Ivanov Anton Aleksandrovich")
                .build();
    }
}
