package ru.centralbank.timetable.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.centralbank.timetable.controller.config.PostControllerTestConfig;
import ru.centralbank.timetable.dto.PostDto;
import ru.centralbank.timetable.service.PostService;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PostControllerTestConfig.class)
@WebAppConfiguration
public class PostControllerTest {
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PostService postService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        initMocks(this);

    }

    @Test
    public void testAddPost_success() throws Exception {
        PostDto postDto = providePostDTO();
        when(postService.addPost(postDto)).thenReturn(1);
        mockMvc.perform(post("/post")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(postDto)))
                .andExpect(status().isOk());
    }

    @Test
    public void testRemovePostById_Returns200() throws Exception {
        mockMvc.perform(delete("/post/1")).andExpect(status().isOk());
    }

    private PostDto providePostDTO() {
        return PostDto.builder()
                .profession("Doctor")
                .build();
    }
}
