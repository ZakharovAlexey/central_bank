package ru.centralbank.timetable.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.centralbank.timetable.controller.config.EmployeeControllerTestConfig;
import ru.centralbank.timetable.dto.EmployeeDto;
import ru.centralbank.timetable.model.Post;
import ru.centralbank.timetable.service.EmployeeService;
import ru.centralbank.timetable.service.converter.PostDtoConverter;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = EmployeeControllerTestConfig.class)
@WebAppConfiguration
public class EmployeeControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private PostDtoConverter postDtoConverter;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        initMocks(this);

    }

    @Test
    public void testAddEmployee_success() throws Exception {
        EmployeeDto employeeDto = provideEmployeeDTO();
        doReturn(1).when(employeeService).addEmployee(any());
        mockMvc.perform(post("/employee")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(employeeDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(Long.toString(1)));
    }

    @Test
    public void testRemoveEmployeeById_Returns200() throws Exception {
        mockMvc.perform(delete("/employee/1")).andExpect(status().isOk());
    }

    private EmployeeDto provideEmployeeDTO() {
        return EmployeeDto.builder()
                .post(postDtoConverter.convertToDto(getPost()))
                .fullName("Ivanov Anton Aleksandrovich")
                .build();
    }

    private Post getPost() {
        return Post.builder()
                .profession("doctor")
                .build();
    }
}
