package ru.centralbank.timetable.controller.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ru.centralbank.timetable.controller.PostController;
import ru.centralbank.timetable.service.PostService;
import ru.centralbank.timetable.service.converter.EmployeeDtoConverter;
import ru.centralbank.timetable.service.converter.PostDtoConverter;

import static org.mockito.Mockito.mock;

@EnableWebMvc
@Configuration
public class PostControllerTestConfig {

    @Bean
    public PostService postService() {
        return mock(PostService.class);
    }

    @Bean
    public PostDtoConverter postDtoConverter() {
        return new PostDtoConverter();
    }

    @Bean
    public EmployeeDtoConverter employeeDtoConverter() {
        return new EmployeeDtoConverter();
    }

    @Bean
    public PostController postController() {
        return new PostController(postService());
    }

    @Bean
    public ObjectMapper mapper() {
        return new ObjectMapper();
    }
}
