package ru.centralbank.timetable.controller.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ru.centralbank.timetable.controller.EmployeeController;
import ru.centralbank.timetable.service.EmployeeService;
import ru.centralbank.timetable.service.converter.EmployeeDtoConverter;
import ru.centralbank.timetable.service.converter.PostDtoConverter;

import static org.mockito.Mockito.mock;

@Configuration
@EnableWebMvc
public class EmployeeControllerTestConfig {

    @Bean
    public EmployeeService employeeService() {
        return mock(EmployeeService.class);
    }

    @Bean
    public EmployeeController employeeController() {
        return new EmployeeController(employeeService());
    }

    @Bean
    public ObjectMapper mapper() {
        return new ObjectMapper();
    }

    @Bean
    public PostDtoConverter postDtoConverter() {
        return new PostDtoConverter();
    }

    @Bean
    public EmployeeDtoConverter employeeDtoConverter() {
        return new EmployeeDtoConverter();
    }
}

