package ru.centralbank.timetable.controller.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ru.centralbank.timetable.controller.ReportController;
import ru.centralbank.timetable.service.ReportService;
import ru.centralbank.timetable.service.converter.EmployeeDtoConverter;
import ru.centralbank.timetable.service.converter.PostDtoConverter;

import static org.mockito.Mockito.mock;

@EnableWebMvc
@Configuration
public class ReportControllerTestConfig {

    @Bean
    public ReportService reportService() {
        return mock(ReportService.class);
    }

    @Bean
    public PostDtoConverter postDtoConverter() {
        return new PostDtoConverter();
    }

    @Bean
    public ReportController reportController() {
        return new ReportController(reportService());
    }

    @Bean
    public ObjectMapper mapper() {
        return new ObjectMapper();
    }

    @Bean
    public EmployeeDtoConverter employeeDtoConverter() {
        return new EmployeeDtoConverter();
    }
}
