package ru.centralbank.timetable.model;

import lombok.*;

import javax.persistence.*;
import javax.persistence.Table;

@Builder
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EMPLOYEE")
public class Employee {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "POST_ID")
    private Post post;

    @Column(name = "FULL_NAME")
    private String fullName;

}
