package ru.centralbank.timetable.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Builder
@Getter
@Setter
@Entity(name = "REPORT")
public class Report {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "EMPLOYEE_ID")
    private Employee employee;

    @Column(name = "DATE_OF_ABSENT")
    private LocalDate dateOfAbsent;

    @Column(name = "HOW_LONG_TIME_IS_ABSENT")
    private Float howLongTimeIsAbsent;

    @Column(name = "REASON")
    private String reason;

}