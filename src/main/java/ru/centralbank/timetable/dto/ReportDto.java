package ru.centralbank.timetable.dto;

import lombok.*;

import java.time.LocalDate;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReportDto {
    private Integer id;
    private EmployeeDto employee;
    private LocalDate dateOfAbsent;
    private Float howLongTimeIsAbsent;
    private String reason;
}
