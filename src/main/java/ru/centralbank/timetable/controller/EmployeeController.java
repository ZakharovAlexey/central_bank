package ru.centralbank.timetable.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.centralbank.timetable.dto.EmployeeDto;
import ru.centralbank.timetable.service.EmployeeService;

import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public ResponseEntity<Integer> addEmployee(@RequestBody EmployeeDto employeeDto) {
        return ResponseEntity.ok(employeeService.addEmployee(employeeDto));
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> removeEmployeeById(@PathVariable Integer id) {
        employeeService.removeEmployeeById(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    public ResponseEntity<EmployeeDto> getEmployeeById(@PathVariable Integer id) {
        return ResponseEntity.ok(employeeService.getEmployeeById(id));
    }

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public ResponseEntity<List<EmployeeDto>> getEmployeeAll() {
        return ResponseEntity.ok(employeeService.getEmployeeAll());
    }
}
