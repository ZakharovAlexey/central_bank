package ru.centralbank.timetable.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.centralbank.timetable.dto.ReportDto;
import ru.centralbank.timetable.service.ReportService;

import java.util.List;

@RestController
@AllArgsConstructor
public class ReportController {

    @Autowired
    private ReportService reportService;

    @RequestMapping(value = "/report", method = RequestMethod.POST)
    public ResponseEntity<Integer> addReport(@RequestBody ReportDto reportDto) {
        return ResponseEntity.ok(reportService.addReport(reportDto));
    }

    @RequestMapping(value = "/report/{id}", method = RequestMethod.DELETE)
    public void removeReport(@PathVariable Integer id) {
        reportService.removeReport(id);
        ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public ResponseEntity<List<ReportDto>> getReportAll() {
        return ResponseEntity.ok(reportService.getReportAll());
    }

    @RequestMapping(value = "/report/employee", method = RequestMethod.GET)
    public ResponseEntity<List<ReportDto>> getReportById(@RequestParam Integer id) {
        return ResponseEntity.ok(reportService.getReportById(id));
    }

    @RequestMapping(value = "/report/post", method = RequestMethod.GET)
    public ResponseEntity<List<ReportDto>> getReportByPostId(@RequestParam Integer id) {
        return ResponseEntity.ok(reportService.getReportByPostId(id));
    }
}

