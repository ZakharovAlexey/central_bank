package ru.centralbank.timetable.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.centralbank.timetable.dto.PostDto;
import ru.centralbank.timetable.service.PostService;

@RestController
@AllArgsConstructor
public class PostController {

    @Autowired
    private PostService postService;

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public ResponseEntity<Integer> addPost(@RequestBody PostDto postDto) {
        return ResponseEntity.ok(postService.addPost(postDto));
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> removePostById(@PathVariable Integer id) {
        postService.removePostById(id);
        return ResponseEntity.ok().build();
    }
}
