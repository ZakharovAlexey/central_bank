package ru.centralbank.timetable.dao;

import org.springframework.data.repository.CrudRepository;
import ru.centralbank.timetable.model.Employee;

public interface EmployeeDao extends CrudRepository<Employee, Integer> {
}
