package ru.centralbank.timetable.dao;

import org.springframework.data.repository.CrudRepository;
import ru.centralbank.timetable.model.Report;

import java.util.List;

public interface ReportDao extends CrudRepository<Report, Integer> {
    List<Report> findAllByEmployeeId(Integer id);

    List<Report> findAllByEmployee_Post_Id(Integer id);
}
