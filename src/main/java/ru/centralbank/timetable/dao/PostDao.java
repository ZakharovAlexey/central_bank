package ru.centralbank.timetable.dao;

import org.springframework.data.repository.CrudRepository;
import ru.centralbank.timetable.model.Post;

public interface PostDao extends CrudRepository<Post, Integer> {
}
