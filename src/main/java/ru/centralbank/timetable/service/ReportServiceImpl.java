package ru.centralbank.timetable.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.centralbank.timetable.dao.ReportDao;
import ru.centralbank.timetable.dto.ReportDto;
import ru.centralbank.timetable.service.converter.ReportDtoConverter;

import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportDao reportDao;
    @Autowired
    private ReportDtoConverter reportDtoConverter;

    @Override
    public Integer addReport(ReportDto reportDto) {
        return reportDao.save(reportDtoConverter.convertFromDto(reportDto)).getId();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void removeReport(Integer id) {
        reportDao.deleteById(id);
    }

    @Override
    public List<ReportDto> getReportAll() {
        return reportDtoConverter.convertToListDto(reportDao.findAll());
    }

    @Override
    public List<ReportDto> getReportById(Integer id) {
        return reportDtoConverter.convertToListDto(reportDao.findAllByEmployeeId(id));
    }

    @Override
    public List<ReportDto> getReportByPostId(Integer id) {
        return reportDtoConverter.convertToListDto(reportDao.findAllByEmployee_Post_Id(id));
    }
}
