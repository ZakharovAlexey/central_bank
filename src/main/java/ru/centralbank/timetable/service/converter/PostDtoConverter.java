package ru.centralbank.timetable.service.converter;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import ru.centralbank.timetable.dto.PostDto;
import ru.centralbank.timetable.model.Post;

@Component
@NoArgsConstructor
public class PostDtoConverter {

    public PostDto convertToDto(Post post) {
        return PostDto.builder()
                .id(post.getId())
                .profession(post.getProfession())
                .build();
    }

    public Post convertFromDto(PostDto postDto) {
        return Post.builder()
                .id(postDto.getId())
                .profession(postDto.getProfession())
                .build();
    }
}
