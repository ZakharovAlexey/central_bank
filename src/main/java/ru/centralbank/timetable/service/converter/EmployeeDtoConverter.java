package ru.centralbank.timetable.service.converter;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.centralbank.timetable.dto.EmployeeDto;
import ru.centralbank.timetable.model.Employee;

import java.util.ArrayList;
import java.util.List;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDtoConverter {

    @Autowired
    private PostDtoConverter postDtoConverter;

    public EmployeeDto convertToDto(Employee employee) {
        return EmployeeDto.builder()
                .id(employee.getId())
                .post(postDtoConverter.convertToDto(employee.getPost()))
                .fullName(employee.getFullName())
                .build();
    }

    public Employee convertFromDto(EmployeeDto employeeDto) {
        return Employee.builder()
                .id(employeeDto.getId())
                .post(postDtoConverter.convertFromDto(employeeDto.getPost()))
                .fullName(employeeDto.getFullName())
                .build();
    }

    public List<EmployeeDto> convertToListDto(Iterable<Employee> list) {
        List<EmployeeDto> listDto = new ArrayList<>();
        list.forEach(e -> listDto.add(convertToDto(e)));
        return listDto;
    }
}


