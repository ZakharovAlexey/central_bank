package ru.centralbank.timetable.service.converter;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.centralbank.timetable.dto.ReportDto;
import ru.centralbank.timetable.model.Report;

import java.util.ArrayList;
import java.util.List;

@Component
@NoArgsConstructor
public class ReportDtoConverter {
    @Autowired
    private EmployeeDtoConverter employeeDtoConverter;

    public ReportDto convertToDto(Report report) {
        return ReportDto.builder()
                .id(report.getId())
                .employee(employeeDtoConverter.convertToDto(report.getEmployee()))
                .dateOfAbsent(report.getDateOfAbsent())
                .howLongTimeIsAbsent(report.getHowLongTimeIsAbsent())
                .reason(report.getReason())
                .build();
    }

    public Report convertFromDto(ReportDto reportDto) {
        return Report.builder()
                .id(reportDto.getId())
                .employee(employeeDtoConverter.convertFromDto(reportDto.getEmployee()))
                .dateOfAbsent(reportDto.getDateOfAbsent())
                .howLongTimeIsAbsent(reportDto.getHowLongTimeIsAbsent())
                .reason(reportDto.getReason())
                .build();
    }

    public List<ReportDto> convertToListDto(Iterable<Report> list) {
        List<ReportDto> listDto = new ArrayList<>();
        list.forEach(e -> listDto.add(convertToDto(e)));
        return listDto;
    }

}
