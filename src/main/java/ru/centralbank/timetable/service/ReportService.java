package ru.centralbank.timetable.service;

import ru.centralbank.timetable.dto.ReportDto;

import java.util.List;

public interface ReportService {

    Integer addReport(ReportDto report);

    void removeReport(Integer id);

    List<ReportDto> getReportAll();

    List<ReportDto> getReportById(Integer id);

    List<ReportDto> getReportByPostId(Integer id);
}
