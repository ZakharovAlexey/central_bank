package ru.centralbank.timetable.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.centralbank.timetable.dao.EmployeeDao;
import ru.centralbank.timetable.dto.EmployeeDto;
import ru.centralbank.timetable.service.converter.EmployeeDtoConverter;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private EmployeeDtoConverter employeeDtoConverter;

    @Override
    public Integer addEmployee(EmployeeDto employeeDto) {
        return employeeDao.save(employeeDtoConverter.convertFromDto(employeeDto)).getId();
    }

    @Override
    public void removeEmployeeById(Integer id) {
        employeeDao.deleteById(id);
    }

    @Override
    public EmployeeDto getEmployeeById(Integer id) {
        return employeeDtoConverter.convertToDto(employeeDao.findById(id)
                .orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public List<EmployeeDto> getEmployeeAll() {
        return employeeDtoConverter.convertToListDto(employeeDao.findAll());
    }

}
