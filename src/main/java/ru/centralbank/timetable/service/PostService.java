package ru.centralbank.timetable.service;

import ru.centralbank.timetable.dto.PostDto;

public interface PostService {
    Integer addPost(PostDto post);

    void removePostById(Integer id);
}
