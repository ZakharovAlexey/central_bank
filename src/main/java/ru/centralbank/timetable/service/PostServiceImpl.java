package ru.centralbank.timetable.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.centralbank.timetable.dao.PostDao;
import ru.centralbank.timetable.dto.PostDto;
import ru.centralbank.timetable.service.converter.PostDtoConverter;

@Service
@Transactional
@AllArgsConstructor
public class PostServiceImpl implements PostService {

    @Autowired
    private PostDtoConverter postDtoConverter;
    @Autowired
    private PostDao postDao;

    @Override
    public Integer addPost(PostDto postDto) {
        return postDao.save(postDtoConverter.convertFromDto(postDto)).getId();

    }

    @SuppressWarnings("unchecked")
    @Override
    public void removePostById(Integer id) {
        postDao.deleteById(id);
    }
}
