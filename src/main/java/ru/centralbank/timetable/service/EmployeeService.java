package ru.centralbank.timetable.service;

import ru.centralbank.timetable.dto.EmployeeDto;

import java.util.List;

public interface EmployeeService {
    Integer addEmployee(EmployeeDto employeeDto);

    void removeEmployeeById(Integer id);

    EmployeeDto getEmployeeById(Integer id);

    List<EmployeeDto> getEmployeeAll();
}
